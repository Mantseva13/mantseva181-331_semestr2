#ifndef PRIVATE_KABINET_H
#define PRIVATE_KABINET_H

#include <QDialog>

namespace Ui {
class private_kabinet;
}

class private_kabinet : public QDialog
{
    Q_OBJECT

public:
    explicit private_kabinet(QWidget *parent = nullptr);
    ~private_kabinet();

    QString new_login;
    QString new_password;
    QString double_password;
    QString old_dannie;
    QString password;
    QString login;
    QString first_dannie;
    QString Qstrok, Nstrok, Qway;
    std::string way;
    QString status;
    QString required_login;
    std::vector <QString> list_all_user;
    std::vector <QString> list_global;

private slots:
    void on_lineEdit_textChanged(const QString &arg1);

    void on_pushButton_clicked();

    void on_lineEdit_3_textChanged(const QString &arg1);

    void on_lineEdit_2_textChanged(const QString &arg1);

    void on_pushButton_2_clicked();


private:
    Ui::private_kabinet *ui;
};

#endif // PRIVATE_KABINET_H

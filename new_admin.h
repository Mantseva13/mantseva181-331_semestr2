#ifndef NEW_ADMIN_H
#define NEW_ADMIN_H

#include <QDialog>
#include "string.h"

namespace Ui {
class new_admin;
}
using namespace std;
class new_admin : public QDialog
{
    Q_OBJECT

public:
    explicit new_admin(QWidget *parent = nullptr);
    ~new_admin();

    QString reg_fio;
    QString reg_login;
    QString reg_password;
    QString dabl_password;
    QString reg_mail;
    QString reg_telefon;

    static int id();

private slots:
    void on_pushButton_clicked();

    void on_lineEdit_textChanged(const QString &arg1);

    void on_lineEdit_2_textChanged(const QString &arg1);

    void on_lineEdit_3_textChanged(const QString &arg1);

    void on_lineEdit_4_textChanged(const QString &arg1);

    void on_lineEdit_5_textChanged(const QString &arg1);

    void on_lineEdit_6_textChanged(const QString &arg1);

private:
    Ui::new_admin *ui;
};

#endif // NEW_ADMIN_H

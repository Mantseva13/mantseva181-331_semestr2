#include "stendi.h"
#include "ui_stendi.h"
#include "list_ex.h"
#include "list_st.h"
#include "MainWindow.h"
#include "redakt_st.h"
#include "private_kabinet.h"
#include <QFile>
#include <QTextStream>

stendi::stendi(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::stendi)
{
    ui->setupUi(this);
}

stendi::~stendi()
{
    delete ui;
}

void stendi::on_pushButton_5_clicked() // выход
{
    this->hide();
    MainWindow *first_window = new MainWindow();
    first_window->show();
}

void stendi::on_pushButton_3_clicked() // список стендов
{
    list_st window;
    window.setModal(true);
    window.exec();
}

void stendi::on_pushButton_2_clicked() // список экскурсий
{
    list_ex window;
    window.setModal(true);
    window.exec();
}

void stendi::on_pushButton_4_clicked() // редактировать стенд
{
    redakt_st window;
    window.setModal(true);
    window.exec();
}

void stendi::on_pushButton_clicked() // личный кабинет
{
    QFile me("C:\\mantseva-181-331-2semestr\\181-331_Mantseva_Lab1\\my_status.txt"); // замена нынешней информации
    if (me.open(QIODevice::WriteOnly | QIODevice::Text)){
        QTextStream write(&me);
         write << "stendist";
        me.close();
    }

    private_kabinet window;
    window.setModal(true);
    window.exec();
}

#ifndef LIST_ST_H
#define LIST_ST_H

#include <QDialog>
#include <QStandardItemModel>
#include <QStandardItem>
#include "string.h"


namespace Ui {
class list_st;
}

using namespace std;
struct form_stendov{
string person;
string name;
string day;
string month;
string year;
string time;

};

int str_to_int(string str);

class list_st : public QDialog
{
    Q_OBJECT

public:
    explicit list_st(QWidget *parent = nullptr);
    ~list_st();

private:
    Ui::list_st *ui;
    QStandardItemModel *tab;
};

#endif // LIST_ST_H

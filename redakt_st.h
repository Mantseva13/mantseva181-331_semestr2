#ifndef REDAKT_ST_H
#define REDAKT_ST_H

#include <QDialog>

namespace Ui {
class redakt_st;
}

using namespace std;

class redakt_st : public QDialog
{
    Q_OBJECT

public:
    explicit redakt_st(QWidget *parent = nullptr);
    ~redakt_st();


    string person;

    QString _name;
    QString _day;
    QString _month;
    QString _year = "2019";
    QString _time;
    QString Qperson;



private slots:
    void on_pushButton_clicked();

    void on_lineEdit_textChanged(const QString &arg1);

    void on_comboBox_currentTextChanged(const QString &arg1);

    void on_comboBox_2_currentTextChanged(const QString &arg1);

    void on_comboBox_3_currentTextChanged(const QString &arg1);

    void on_lineEdit_2_textChanged(const QString &arg1);


private:
    Ui::redakt_st *ui;
};

#endif // REDAKT_ST_H

#include "private_kabinet.h"
#include "ui_private_kabinet.h"
#include <QMessageBox>
#include <QFile>
#include <QTextStream>
#include "registration.h"
#include <fstream>
#include <iostream>
#include <sstream>
#include <cstdlib>
#include <vector>

private_kabinet::private_kabinet(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::private_kabinet)
{
    int num_info = 1;
    string line, str, s_str;
    std::ifstream file, stat;
    stat.open("C:\\mantseva-181-331-2semestr\\181-331_Mantseva_Lab1\\my_status.txt", std::ios::in);
    while (!stat.eof()) {
        getline(stat,s_str);
        status = QString::fromLocal8Bit(s_str.c_str());
    }
    file.open("C:\\mantseva-181-331-2semestr\\181-331_Mantseva_Lab1\\this_is_me.txt", std::ios::in); // достаем из файла логин и пароль пользователя
    while (!file.eof()){
        getline(file,str);
        istringstream strok(str);

            while(getline(strok, line, '|')){

            if(num_info == 1){
                 first_dannie = QString::fromLocal8Bit(line.c_str());
              }
                if (num_info == 2){
                   login = QString::fromLocal8Bit(line.c_str());
                 }
                if(num_info == 3){
                    password = QString::fromLocal8Bit(line.c_str());
                 }
                if(num_info == 4){
                    old_dannie = QString::fromLocal8Bit(line.c_str());
                 }
                if(num_info > 4){
                    QString ir = QString::fromLocal8Bit(line.c_str());
                    old_dannie = old_dannie + "|" + ir;
                 }
                num_info++;

                }


      }
    ui->setupUi(this);

}

private_kabinet::~private_kabinet()
{
    delete ui;
}

void private_kabinet::on_lineEdit_textChanged(const QString &arg1) // изменение логина
{
    new_login = arg1;

}

void private_kabinet::on_pushButton_clicked() // сохранение логина
{
    list_global.clear();
    list_all_user.clear();
    QFile me("C:\\mantseva-181-331-2semestr\\181-331_Mantseva_Lab1\\this_is_me.txt"); // замена нынешней информации
    if (me.open(QIODevice::WriteOnly | QIODevice::Text)){
        QTextStream write(&me);
        Qstrok = first_dannie + "|" + new_login + "|" + password + "|" + old_dannie + "|";
        write << Qstrok;
        me.close();
    }
    std::ifstream Afile, Dfile;
    int num_info = 1;

      int Dnum_info = 1;
      string line, str, Dline, Dstr;
        Dfile.open("C:\\mantseva-181-331-2semestr\\181-331_Mantseva_Lab1\\users.txt", std::ios::in);
        while (!Dfile.eof()){
            getline(Dfile,Dstr);
            istringstream Dstrok(Dstr);
            while(getline(Dstrok, Dline, '|')){
                    if (Dnum_info == 2){
                        required_login = QString::fromLocal8Bit(Dline.c_str());
                        if (login != required_login){
                            Nstrok = QString::fromLocal8Bit(Dstr.c_str());
                            list_all_user.push_back(Nstrok);
                        }
                    }
                  Dnum_info++;
                }
                 Dnum_info = 1;
              }
        Dfile.close();

        QFile as_user("C:\\mantseva-181-331-2semestr\\181-331_Mantseva_Lab1\\users.txt"); // замена нынешней информации
        if (as_user.open(QIODevice::WriteOnly | QIODevice::Text)){
            QTextStream write(&as_user);
            for (int i = 0; i < list_all_user.size(); i++){
            write << list_all_user[i] << "\n";
            }
            write << status + "|" + new_login + "|" + password + "||\n";

    }
        as_user.close();

        if (status == "stendist"){
           way = "C:\\mantseva-181-331-2semestr\\181-331_Mantseva_Lab1\\stendist.txt";
           Qway = "C:\\mantseva-181-331-2semestr\\181-331_Mantseva_Lab1\\stendist.txt";
        }
        else if (status == "admin") {
             way = "C:\\mantseva-181-331-2semestr\\181-331_Mantseva_Lab1\\adm.txt";
             Qway = "C:\\mantseva-181-331-2semestr\\181-331_Mantseva_Lab1\\adm.txt";
        }
        else if (status == "visitor") {
            way = "C:\\mantseva-181-331-2semestr\\181-331_Mantseva_Lab1\\visit.txt";
            Qway = "C:\\mantseva-181-331-2semestr\\181-331_Mantseva_Lab1\\visit.txt";
        }
        Afile.open(way,std::ios::in);
        while (!Afile.eof()){
            getline(Afile,str);
            istringstream strok(str);
            while(getline(strok, line, '|')){
                    if (num_info == 2){
                        required_login = QString::fromLocal8Bit(line.c_str());
                        if (login != required_login){
                            Nstrok = QString::fromLocal8Bit(str.c_str());
                            list_global.push_back(Nstrok);
                        }
                    }
                  num_info++;
                }
                 num_info = 1;
              }
        Afile.close();
        QFile global_info(Qway);
        if (global_info.open(QIODevice::WriteOnly | QIODevice::Text)){
            QTextStream write(&global_info);
            for (int i = 0; i < list_global.size(); i++){
            write << list_global[i] << "\n";
            }
            write << Qstrok << "\n";
         global_info.close();
    }
        login = new_login;
         QMessageBox::information(this,"Поздравляем!", "Смена логина прошла успешно");
}

void private_kabinet::on_lineEdit_3_textChanged(const QString &arg1) // изменение пароля
{
    new_password = arg1;
}

void private_kabinet::on_lineEdit_2_textChanged(const QString &arg1) // подтверждение пароля
{
    double_password = arg1;
}

void private_kabinet::on_pushButton_2_clicked() // сохранение пароля
{
    if (double_password == new_password){
        list_global.clear();
        list_all_user.clear();
        QFile me("C:\\mantseva-181-331-2semestr\\181-331_Mantseva_Lab1\\this_is_me.txt"); // замена нынешней информации
        if (me.open(QIODevice::WriteOnly | QIODevice::Text)){
            QTextStream write(&me);
            Qstrok = first_dannie + "|" + login + "|" + new_password + "|" + old_dannie + "|";
            write << Qstrok;
            me.close();
        }
        std::ifstream Afile, Dfile;
        int num_info = 1;

          int Dnum_info = 1;
          string line, str, Dline, Dstr;
            Dfile.open("C:\\mantseva-181-331-2semestr\\181-331_Mantseva_Lab1\\users.txt", std::ios::in);
            while (!Dfile.eof()){
                getline(Dfile,Dstr);
                istringstream Dstrok(Dstr);
                while(getline(Dstrok, Dline, '|')){
                        if (Dnum_info == 2){
                            required_login = QString::fromLocal8Bit(Dline.c_str());
                            if (login != required_login){
                                Nstrok = QString::fromLocal8Bit(Dstr.c_str());
                                list_all_user.push_back(Nstrok);
                            }
                        }
                      Dnum_info++;
                    }
                     Dnum_info = 1;
                  }
            Dfile.close();

            QFile as_user("C:\\mantseva-181-331-2semestr\\181-331_Mantseva_Lab1\\users.txt"); // замена нынешней информации
            if (as_user.open(QIODevice::WriteOnly | QIODevice::Text)){
                QTextStream write(&as_user);
                for (int i = 0; i < list_all_user.size(); i++){
                write << list_all_user[i] << "\n";
                }
                write << status + "|" + login + "|" + new_password + "||\n";

        }
            as_user.close();

            if (status == "stendist"){
               way = "C:\\mantseva-181-331-2semestr\\181-331_Mantseva_Lab1\\stendist.txt";
               Qway = "C:\\mantseva-181-331-2semestr\\181-331_Mantseva_Lab1\\stendist.txt";
            }
            else if (status == "admin") {
                 way = "C:\\mantseva-181-331-2semestr\\181-331_Mantseva_Lab1\\adm.txt";
                 Qway = "C:\\mantseva-181-331-2semestr\\181-331_Mantseva_Lab1\\adm.txt";
            }
            else if (status == "visitor") {
                way = "C:\\mantseva-181-331-2semestr\\181-331_Mantseva_Lab1\\visit.txt";
                Qway = "C:\\mantseva-181-331-2semestr\\181-331_Mantseva_Lab1\\visit.txt";
            }
            Afile.open(way,std::ios::in);
            while (!Afile.eof()){
                getline(Afile,str);
                istringstream strok(str);
                while(getline(strok, line, '|')){
                        if (num_info == 2){
                            required_login = QString::fromLocal8Bit(line.c_str());
                            if (login != required_login){
                                Nstrok = QString::fromLocal8Bit(str.c_str());
                                list_global.push_back(Nstrok);
                            }
                        }
                      num_info++;
                    }
                     num_info = 1;
                  }
            Afile.close();
            QFile global_info(Qway);
            if (global_info.open(QIODevice::WriteOnly | QIODevice::Text)){
                QTextStream write(&global_info);
                for (int i = 0; i < list_global.size(); i++){
                write << list_global[i] << "\n";
                }
                write << Qstrok << "\n";
             global_info.close();
        }
            password = new_password;
       QMessageBox::information(this,"Поздравляем!", "Смена пароля прошла успешно");
    }
    else {
        QMessageBox::warning(this, "Ошибка", "Пароли не совпадают. Пожалуйста введите их заново.");
    }
}


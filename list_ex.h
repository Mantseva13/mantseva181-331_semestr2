#ifndef LIST_EX_H
#define LIST_EX_H

#include <QDialog>
#include <QStandardItemModel>
#include <QStandardItem>
#include "string.h"


namespace Ui {
class list_ex;
}

using namespace  std;
struct form_exurs{
    string name;
    string time;
    string money;
};

int str_to_int(string str);

class list_ex : public QDialog
{
    Q_OBJECT

public:
    explicit list_ex(QWidget *parent = nullptr);
    ~list_ex();

private:
    Ui::list_ex *ui;
    QStandardItemModel *tab;
};

#endif // LIST_EX_H

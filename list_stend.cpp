#include "list_stend.h"
#include "ui_list_stend.h"
#include "registration.h"
#include "string.h"
#include <fstream>
#include <iostream>
#include <QModelIndex>
#include <sstream>
#include <cstdlib>

using namespace std;
list_stend::list_stend(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::list_stend)
{
    ui->setupUi(this);
    registration num;
    tab = new QStandardItemModel(num.id("stendist.txt"), 3, this);
    ui->tableView->setModel(tab);
    this->setWindowTitle("Список стендистов");
    tab->setHeaderData(0, Qt::Horizontal, "Имя Фамилия Отчество");
    tab->setHeaderData(1, Qt::Horizontal, "Почта");
    tab->setHeaderData(2, Qt::Horizontal, "Телефон");
    ui->tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    QModelIndex index;
    QString Qinfo;
    int num_info = 1;
    int num_strok = 0;
    string line;
    string str;
    std::ifstream file;
    stendist_dannie info;
    file.open("C:\\mantseva-181-331-2semestr\\181-331_Mantseva_Lab1\\stendist.txt", std::ios::in);
    while (!file.eof()){
        getline(file,str);
        istringstream strok(str);
        while(getline(strok, line, '|')){
                if (num_info == 4){
                   info.FIO = line;
                  Qinfo = QString::fromLocal8Bit(info.FIO.c_str());
                index = tab->index(num_strok, 0);
                tab->setData(index, Qinfo);
                }
                if (num_info == 5) {
                    info.mail = line;
                    Qinfo = QString::fromLocal8Bit(info.mail.c_str());
                    index = tab->index(num_strok, 1);
                    tab->setData(index, Qinfo);
                }
                if (num_info == 6) {
                    info.telefon = line;
                    Qinfo = QString::fromLocal8Bit(info.telefon.c_str());
                    index = tab->index(num_strok, 2);
                    tab->setData(index, Qinfo);
                }
              num_info++;
            }

            num_info = 1;
           num_strok++;
        }
}

list_stend::~list_stend()
{
    delete ui;
}

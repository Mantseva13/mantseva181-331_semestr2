#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "QString"
#include "iostream"
#include "string.h"

using namespace std;


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

protected:
    QString nlogin;
    QString npassword;

    QString fio;
    QString mail;
    QString telefon;

   string _login;
   string _password;
   string _fio;
   string _mail;
   string _telefon;


    Ui::MainWindow *ui;
public:


    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    string getlog(){
        return _login;
    }
    string getpass(){
        return _password;
    }

    int _id(string name_file);


    void registrat(string pass, string log, string fmi, string imail, string tel);

private slots:
    void on_lineEdit_3_textEdited(const QString &arg1);

    void on_lineEdit_2_textEdited(const QString &arg1);

    void on_pushButton_2_clicked();

    void on_pushButton_clicked();
};

class Stendist : public MainWindow{
private:
    QString namest;
    QString time;
    QString data;

    string _namest = namest.toUtf8().constData();
    string _time = time.toUtf8().constData();
    string _data = data.toUtf8().constData();

public:
    void stend(QString tm, QString name, QString gt); // стендист выбирает время, название и дату своего стенда

};





#endif // MAINWINDOW_H

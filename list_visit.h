#ifndef LIST_VISIT_H
#define LIST_VISIT_H

#include <QDialog>
#include <QStandardItemModel>
#include <QStandardItem>
#include "string.h"

namespace Ui {
class list_visit;
}
using namespace std;

struct user_dannie{
     string FIO;
     string mail;
     string telefon;
     string pass;
     string log;
};


class list_visit : public QDialog
{
    Q_OBJECT

public:
    explicit list_visit(QWidget *parent = nullptr);
    ~list_visit();

private:
    Ui::list_visit *ui;
    QStandardItemModel *tab;
};

#endif // LIST_VISIT_H

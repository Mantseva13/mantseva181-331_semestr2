#include "admin.h"
#include "ui_admin.h"
#include "list_visit.h"
#include "excurs.h"
#include "list_stend.h"
#include "new_admin.h"
#include "list_ex.h"
#include "list_st.h"
#include "MainWindow.h"
#include "private_kabinet.h"
#include <QFile>
#include <QTextStream>

admin::admin(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::admin)
{
    ui->setupUi(this);
}

admin::~admin()
{
    delete ui;
}

void admin::on_pushButton_7_clicked() // кнопка выхода
{
    this->hide();
    MainWindow *first_window = new MainWindow();
    first_window->show();
}

void admin::on_pushButton_6_clicked()  //список посетителей
{
    list_visit window;
    window.setModal(true);
    window.exec();
}

void admin::on_pushButton_3_clicked() // список стендистов
{
    list_stend window;
    window.setModal(true);
    window.exec();
}

void admin::on_pushButton_5_clicked() // добавление нового администратора
{
    new_admin window;
    window.setModal(true);
    window.exec();
}

void admin::on_pushButton_8_clicked() // добавление новой экскурсии
{
    excurs window;
    window.setModal(true);
    window.exec();
}

void admin::on_pushButton_2_clicked() // список экскурсий
{
    list_ex window;
    window.setModal(true);
    window.exec();
}

void admin::on_pushButton_4_clicked() // личный кабинет
{
    QFile me("C:\\mantseva-181-331-2semestr\\181-331_Mantseva_Lab1\\my_status.txt");
    if (me.open(QIODevice::WriteOnly | QIODevice::Text)){
        QTextStream write(&me);
         write << "admin";

    }
    me.close();

    private_kabinet window;
    window.setModal(true);
    window.exec();
}

#include "MainWindow.h"
#include "ui_MainWindow.h"
#include "QString"
#include "QMessageBox"
#include "registration.h"
#include "autorization.h"
#include "string.h"
#include <fstream>
#include <iostream>
#include "stendi.h"
#include "admin.h"
#include <QFile>
#include <QTextStream>
#include "list_visit.h"
#include "list_stend.h"
#include "admin.h"
#include <sstream>
#include <cstdlib>





MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

int MainWindow::_id(string name_file){ // подсчёт строк в файле

    string sr;
    int k = 0;
    std::ifstream file;
    file.open("C:\\mantseva-181-331-2semestr\\181-331_Mantseva_Lab1\\"+name_file, std::ios_base::in);
    while(getline(file, sr)){
        k++;
    }

    return k;
}



void MainWindow::on_lineEdit_3_textEdited(const QString &arg1)
{
    nlogin = arg1;
    _login = nlogin.toUtf8().constData();
}

void MainWindow::on_lineEdit_2_textEdited(const QString &arg1)
{
    npassword = arg1;
    _password = npassword.toUtf8().constData();
}


void MainWindow::on_pushButton_2_clicked() // кнопка входа
{
    bool a = false;
    string line;
    std::ifstream file;
    file.open("C:\\mantseva-181-331-2semestr\\181-331_Mantseva_Lab1\\users.txt", std::ios::in);
    while (!file.eof()){

        getline(file, line);

        if(line == ("visitor|"+_login+"|"+_password+"||")){ // входим как посетитель
            //MainWindow *host = new MainWindow;
           QFile me("C:\\mantseva-181-331-2semestr\\181-331_Mantseva_Lab1\\this_is_me.txt");
           int num_info = 1;
           int num_strok = 0;
           int flag = 0;
           string line, str;
           std::ifstream file;
           QString Qinfo_log, Qinfo_pass, Qstrok;
           user_dannie info;
           if (me.open(QIODevice::WriteOnly | QIODevice::Text)){
           file.open("C:\\mantseva-181-331-2semestr\\181-331_Mantseva_Lab1\\visit.txt", std::ios::in);
           while (!file.eof()){
               getline(file,str);
               istringstream strok(str);
               while(getline(strok, line, '|')){
                       if (num_info == 2){
                          info.log = line;
                          Qinfo_log = QString::fromLocal8Bit(info.log.c_str());
                          if (info.log == _login){
                              flag++;
                          }

                       }
                       if (num_info == 3) {
                           info.pass = line;
                           Qinfo_pass = QString::fromLocal8Bit(info.pass.c_str());
                           if (info.pass == _password){
                               flag++;
                           }

                       }

                     num_info++;
                   }
                  if(flag == 2){
                      Qstrok = QString::fromLocal8Bit(str.c_str());
                      break;
                  }
                  else {
                      flag = 0;
                      num_info = 1;
                      num_strok++;
                  }
                 }
           QTextStream write(&me);
           write << Qstrok << "\n";
           me.close();
           }
            this->hide();
            autorization hod;
            a = true;
            hod.setModal(true);
            hod.exec();
        }
        if(line == ("stendist|"+_login+"|"+_password+"||")){ // входим как стендист
            QFile me("C:\\mantseva-181-331-2semestr\\181-331_Mantseva_Lab1\\this_is_me.txt");
            int num_info = 1;
            int num_strok = 0;
            int flag = 0;
            string line, str;
            std::ifstream file;
            QString Qinfo_log, Qinfo_pass, Qstrok;
            user_dannie info;
            if (me.open(QIODevice::WriteOnly | QIODevice::Text)){
            file.open("C:\\mantseva-181-331-2semestr\\181-331_Mantseva_Lab1\\stendist.txt", std::ios::in);
            while (!file.eof()){
                getline(file,str);
                istringstream strok(str);
                while(getline(strok, line, '|')){
                        if (num_info == 2){
                           info.log = line;
                           Qinfo_log = QString::fromLocal8Bit(info.log.c_str());
                           if (info.log == _login){
                               flag++;
                           }

                        }
                        if (num_info == 3) {
                            info.pass = line;
                            Qinfo_pass = QString::fromLocal8Bit(info.pass.c_str());
                            if (info.pass == _password){
                                flag++;
                            }

                        }

                      num_info++;
                    }
                   if(flag == 2){
                       Qstrok = QString::fromLocal8Bit(str.c_str());
                       break;
                   }
                   else {
                       flag = 0;
                       num_info = 1;
                       num_strok++;
                   }
                  }
            QTextStream write(&me);
            write << Qstrok << "\n";
            me.close();
            }

            this->hide();
            stendi hod;
            a = true;
            hod.setModal(true);
            hod.exec();
        }
        if(line == ("admin|"+_login+"|"+_password+"||")){ // входим как администатор
            QFile me("C:\\mantseva-181-331-2semestr\\181-331_Mantseva_Lab1\\this_is_me.txt");
            int num_info = 1;
            int num_strok = 0;
            int flag = 0;
            string line, str;
            std::ifstream file;
            QString Qinfo_log, Qinfo_pass, Qstrok;
            user_dannie info;
            if (me.open(QIODevice::WriteOnly | QIODevice::Text)){
            file.open("C:\\mantseva-181-331-2semestr\\181-331_Mantseva_Lab1\\adm.txt", std::ios::in);
            while (!file.eof()){
                getline(file,str);
                istringstream strok(str);
                while(getline(strok, line, '|')){
                        if (num_info == 2){
                           info.log = line;
                           Qinfo_log = QString::fromLocal8Bit(info.log.c_str());
                           if (info.log == _login){
                               flag++;
                           }

                        }
                        if (num_info == 3) {
                            info.pass = line;
                            Qinfo_pass = QString::fromLocal8Bit(info.pass.c_str());
                            if (info.pass == _password){
                                flag++;
                            }

                        }

                      num_info++;
                    }
                   if(flag == 2){
                       Qstrok = QString::fromLocal8Bit(str.c_str());
                       break;
                   }
                   else {
                       flag = 0;
                       num_info = 1;
                       num_strok++;
                   }
                  }
            QTextStream write(&me);
            write << Qstrok << "\n";
            me.close();
            }

            this->hide();
            admin hod;
            a = true;
            hod.setModal(true);
            hod.exec();
        }
        else if(file.eof()&& a==false) {
            QMessageBox::warning(this, "Ошибка авторизации", "Логин или пароль введены неверно. Повторите вход");
        }

    }
    file.close();
  }



void MainWindow::on_pushButton_clicked(){ // кнопка регистрации
    //this -> hide();
    registration vhod;
    vhod.setModal(true);
    vhod.exec();
}

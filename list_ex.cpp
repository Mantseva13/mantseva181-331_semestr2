#include "list_ex.h"
#include "ui_list_ex.h"
#include "string.h"
#include <fstream>
#include <iostream>
#include <QModelIndex>
#include <sstream>
#include <cstdlib>
#include "registration.h"

using namespace std;

list_ex::list_ex(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::list_ex)
{
    ui->setupUi(this);
    registration num;
    tab = new QStandardItemModel(num.id("excurs.txt"), 3, this);
    ui->tableView->setModel(tab);
    this->setWindowTitle("Список экскурсий");
    tab->setHeaderData(0, Qt::Horizontal, "Название экскурсии");
    tab->setHeaderData(1, Qt::Horizontal, "Время (в минутах)");
    tab->setHeaderData(2, Qt::Horizontal, "Цена (в рублях)");
    ui->tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    QModelIndex index;
    QString Qinfo;
    QString Qinfo2;
    QString Qinfo3;
    int num_info = 1;
    int num_strok = 0;
    string line, str;
    std::ifstream file;
    form_exurs info;

    file.open("C:\\mantseva-181-331-2semestr\\181-331_Mantseva_Lab1\\excurs.txt", std::ios::in);
    while (!file.eof()){
        getline(file,str);
        istringstream strok(str);
        while(getline(strok, line, '|')){
                if (num_info == 2){
                   info.name = line;
                  Qinfo = QString::fromLocal8Bit(info.name.c_str());
                index = tab->index(num_strok, 0);
                tab->setData(index, Qinfo);
                }
                if (num_info == 3) {
                    info.time = line;
                    Qinfo2 = QString::fromLocal8Bit(info.time.c_str());
                   index = tab->index(num_strok, 1);
                    tab->setData(index, Qinfo2);
                }
                if (num_info == 4) {
                    info.money = line;
                    Qinfo3 = QString::fromLocal8Bit(info.money.c_str());
                    index = tab->index(num_strok, 2);
                    tab->setData(index, Qinfo3);
                }
              num_info++;
            }

            num_info = 1;
           num_strok++;
          }

}

int str_to_int(string str){
    stringstream ss;
    int result;
    if (str.empty())
        return 0;
    ss << str;
    ss >> result;
    return result;
}

list_ex::~list_ex()
{
    delete ui;
}

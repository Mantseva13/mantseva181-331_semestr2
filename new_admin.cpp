#include "new_admin.h"
#include "ui_new_admin.h"
#include "MainWindow.h"
#include "string.h"
#include <string>
#include <QMessageBox>
#include <QFile>
#include <QTextStream>
#include <fstream>
#include <iostream>

new_admin::new_admin(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::new_admin)
{
    ui->setupUi(this);
}

new_admin::~new_admin()
{
    delete ui;
}

int new_admin::id(){

   string sr;
   int k = 0;
   std::ifstream file;
   file.open("C:\\mantseva-181-331-2semestr\\181-331_Mantseva_Lab1\\adm.txt", std::ios_base::in);
   while(getline(file, sr)){
       k++;
   }

   return k;
};

void new_admin::on_lineEdit_textChanged(const QString &arg1) // ФИО
{
   reg_fio = arg1;
}

void new_admin::on_lineEdit_2_textChanged(const QString &arg1) // почта
{
    reg_mail = arg1;
}

void new_admin::on_lineEdit_3_textChanged(const QString &arg1) // телефон
{
    reg_telefon = arg1;
}

void new_admin::on_lineEdit_4_textChanged(const QString &arg1) // логин
{
    reg_login = arg1;
}

void new_admin::on_lineEdit_5_textChanged(const QString &arg1) // пароль
{
    reg_password = arg1;

}

void new_admin::on_lineEdit_6_textChanged(const QString &arg1) // повтор пароля
{
     dabl_password = arg1;
}

void new_admin::on_pushButton_clicked() // кнопка добавления нового администратора
{
    if (reg_password != dabl_password){
        QMessageBox::warning(this, "Ошибка", "Пароли не совпадают. Пожалуйста введите их заново.");
    }
    else {
//            std::ofstream administrat;
//            administrat.open("C:\\mantseva-181-331-2semestr\\181-331_Mantseva_Lab1\\adm.txt", std::ios_base::out | std::ios_base::app);
//            administrat << this->id()+1<<"|"<<_reg_login<<"|"<<_reg_password<<"|"<<_reg_fio<<"|"<<_reg_mail<<"|"<<_reg_telefon<<"||\n";
//            administrat.close();
//            std::ofstream user;
//            user.open("C:\\mantseva-181-331-2semestr\\181-331_Mantseva_Lab1\\users.txt",std::ios_base::out | std::ios_base::app);
//            user << "admin|"<<_reg_login<<"|"<<_reg_password<<"||\n";
//            user.close();
        QFile administrat("C:\\mantseva-181-331-2semestr\\181-331_Mantseva_Lab1\\adm.txt");
        if (administrat.open(QIODevice::Append | QIODevice::Text)){
            QTextStream write(&administrat);
            write << this->id()+1<<"|"<<reg_login<<"|"<<reg_password<<"|"<<reg_fio<<"|"<<reg_mail<<"|"<<reg_telefon<<"||\n";
            administrat.close();
        }

        QFile all("C:\\mantseva-181-331-2semestr\\181-331_Mantseva_Lab1\\users.txt");
        if (all.open( QIODevice::Append | QIODevice::Text)){
            QTextStream rite(&all);
            rite << "admin|"<<reg_login<<"|"<<reg_password<<"||\n";
            all.close();
        }

            QMessageBox::information(this,"Поздравляем!", "Новый администратор успешно зарегистрирован. Теперь он может войти в свою учётную запись");

        }
}

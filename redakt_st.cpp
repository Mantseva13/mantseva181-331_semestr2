#include "redakt_st.h"
#include "ui_redakt_st.h"
#include <QMessageBox>
#include <QFile>
#include <QTextStream>
#include "registration.h"
#include <fstream>
#include <iostream>
#include <sstream>
#include <cstdlib>


redakt_st::redakt_st(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::redakt_st)
{

       int num_info = 1;
       string line, str;
       std::ifstream file;

       file.open("C:\\mantseva-181-331-2semestr\\181-331_Mantseva_Lab1\\this_is_me.txt", std::ios::in); // достаем из файла логин пользователя
       while (!file.eof()){
           getline(file,str);
           istringstream strok(str);
           while(getline(strok, line, '|')){
                   if (num_info == 4){
                      person = line;
                     Qperson = QString::fromLocal8Bit(person.c_str());
                    }
                   num_info++;
                   }
             }

    ui->setupUi(this);
}

redakt_st::~redakt_st()
{
    delete ui;
}

void redakt_st::on_lineEdit_textChanged(const QString &arg1) // название
{
    _name = arg1;
}


void redakt_st::on_comboBox_currentTextChanged(const QString &arg1) // изменение дня
{
    _day = arg1;
}

void redakt_st::on_comboBox_2_currentTextChanged(const QString &arg1) // изменение месяца
{
    _month = arg1;
}

void redakt_st::on_comboBox_3_currentTextChanged(const QString &arg1) // изменение года
{
    _year = arg1;
}

void redakt_st::on_pushButton_clicked() // кнопка сохранения
{

    QFile new_stend("C:\\mantseva-181-331-2semestr\\181-331_Mantseva_Lab1\\red_st.txt");
    if (new_stend.open(QIODevice::Append | QIODevice::Text)){
        QTextStream write(&new_stend);
            write << Qperson << "|" << _name << "|" << _day << "|" << _month << "|" << _year << "|" << _time << "||\n";

    }
    QMessageBox::information(this,"Поздравляем!", "Новый стэнд успешно добавлен");
    new_stend.close();
}


void redakt_st::on_lineEdit_2_textChanged(const QString &arg1) // изменение времени
{
    _time = arg1;
}

#include <QCoreApplication>
#include <QtSql>

static bool createConnection()
{
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName("addressbook");
    if (!db.open()) {
        qDebug() << "Cannot open database:" << db.lastError().text();
        return false;
    }
    return true;
}


int main(int argc, char** argv)
{
    QCoreApplication app(argc, argv);
    // Соединяемся с менеджером баз данных
    if (!createConnection()) {
        return -1;
    }
    // Создаем базу
    QSqlQuery query;
    QString str = "CREATE TABLE Tablitschka (number INTEGER PRIMARY KEY NOT NULL, name VARCHAR(15), phone VARCHAR(12), email VARCHAR(15));";
    if (!query.exec(str)) {
        qDebug() << "Unable to create a table";
    }
    // Добавляем данные в базу
    QString strF =
            "INSERT INTO Tablitschka (number, name, phone, email) "
            "VALUES(%1, '%2', '%3', '%4');";
    str = strF.arg("11")
            .arg("Vasa")
            .arg("+7 986 453 42 13")
            .arg("ollan@mail.ru");
    if (!query.exec(str)) {
        qDebug() << "Unable to make insert operation";
    }
    str = strF.arg("12")
            .arg("Mila")
            .arg("+7 916 230 00 45")
            .arg("belov@list.ru");
    if (!query.exec(str)) {
        qDebug() << "Unable to make insert operation";
    }
    if (!query.exec("SELECT * FROM Tablitschka;")) {
        qDebug() << "Unable to execute query — exiting";
        return 1;
    }
    // Считываем данные из базы
    QSqlRecord rec = query.record();
    int nNumber = 0;
    QString strName;
    QString strPhone;
    QString strEmail;
    while (query.next()) {
        nNumber = query.value(rec.indexOf("number")).toInt();
        strName = query.value(rec.indexOf("name")).toString();
        strPhone = query.value(rec.indexOf("phone")).toString();
        strEmail = query.value(rec.indexOf("email")).toString();

        qDebug() << nNumber << " " << strName << ";\t" << strPhone << ";\t" << strEmail;
    }
    qDebug() << query.exec("SELECT COUNT(*) FROM Tablitschka"); // должна считать строки, но не считает

    return 0;
}


#ifndef STENDI_H
#define STENDI_H

#include <QDialog>

namespace Ui {
class stendi;
}

class stendi : public QDialog
{
    Q_OBJECT

public:
    explicit stendi(QWidget *parent = nullptr);
    ~stendi();

private slots:
    void on_pushButton_5_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_4_clicked();

    void on_pushButton_clicked();

private:
    Ui::stendi *ui;
};

#endif // STENDI_H

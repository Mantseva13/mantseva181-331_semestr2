#include "registration.h"
#include "ui_registration.h"
#include "MainWindow.h"
#include "string.h"
#include <string>
#include <QMessageBox>
#include <fstream>
#include <iostream>

#include <QFile>
#include <QTextStream>



using namespace std;
registration::registration(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::registration)
{
    ui->setupUi(this);
}

registration::~registration()
{
    delete ui;
}

 int registration::id(string name_file){

    string sr;
    int k = 0;
    std::ifstream file;
    file.open("C:\\mantseva-181-331-2semestr\\181-331_Mantseva_Lab1\\"+name_file, std::ios_base::in);
    while(getline(file, sr)){
        k++;
    }

    return k;
};

void registration::on_lineEdit_textChanged(const QString &arg1) // ФИО
{
    reg_fio = arg1;
}

void registration::on_lineEdit_2_textChanged(const QString &arg1) // почта
{
    reg_mail = arg1;

}

void registration::on_lineEdit_4_textChanged(const QString &arg1) // телефон
{
    reg_telefon = arg1;
}

void registration::on_lineEdit_3_textChanged(const QString &arg1) // логин
{
    reg_login = arg1;

}

void registration::on_lineEdit_5_textChanged(const QString &arg1) // пароль
{
    reg_password = arg1;

}

void registration::on_radioButton_clicked(bool checked) // выборка стендист
{
    if (checked == true){
       Stendist nov;
       shost = &nov;
       vis = 1; // значит это стендист
    }
}

void registration::on_radioButton_2_clicked(bool checked) // выборка посетитель
{
    if (checked == true){
        MainWindow nova;
        host = &nova;
        vis = 2;   // это посетитель
    }
}

void registration::on_lineEdit_6_textChanged(const QString &arg1) // повторный пароль
{
    dabl_password = arg1;
}

void registration::on_pushButton_clicked() // кнопка регистрации и сохранение данных
{
    if (reg_password != dabl_password){
        QMessageBox::warning(this, "Ошибка", "Пароли не совпадают. Пожалуйста введите их заново.");
    }
    else {

        if (vis==1){ // стендист
            //shost->registrat(_reg_password, _reg_login, _reg_fio, _reg_mail, _reg_telefon);

//            std::ofstream stendi;
//            stendi.open("C:\\mantseva-181-331-2semestr\\181-331_Mantseva_Lab1\\stendist.txt", std::ios_base::out | std::ios_base::app);
//            stendi <<this->id("stendist.txt")+1<<"|"<<_reg_login<<"|"<<_reg_password<<"|"<<_reg_fio<<"|"<<_reg_mail<<"|"<<_reg_telefon<<"||\n";
//            stendi.close();
//            std::ofstream user;
//            user.open("C:\\mantseva-181-331-2semestr\\181-331_Mantseva_Lab1\\users.txt",std::ios_base::out | std::ios_base::app);
//            user << "stendist|"<<_reg_login<<"|"<<_reg_password<<"||\n";
//            user.close();
            QFile stendi("C:\\mantseva-181-331-2semestr\\181-331_Mantseva_Lab1\\stendist.txt");
            if (stendi.open(QIODevice::Append | QIODevice::Text)){
                QTextStream write(&stendi);
                write << this->id("stendist.txt")+1<<"|"<<reg_login<<"|"<<reg_password<<"|"<<reg_fio<<"|"<<reg_mail<<"|"<<reg_telefon<<"||\n";
                stendi.close();
            }
            QFile all("C:\\mantseva-181-331-2semestr\\181-331_Mantseva_Lab1\\users.txt");
            if (all.open( QIODevice::Append | QIODevice::Text)){
                QTextStream rite(&all);
                rite << "stendist|"<<reg_login<<"|"<<reg_password<<"||\n";
                all.close();
            }
            QMessageBox::information(this,"Поздравляем!", "Вы успешно зарегистрировались на выставку. Теперь вы можете войти в свою учётную запись");
        }

    else if (vis ==2) { // посетитель
            //host->registrat(_reg_password, _reg_login, _reg_fio, _reg_mail, _reg_telefon);

//             std::ofstream visitor;
//             visitor.open("C:\\mantseva-181-331-2semestr\\181-331_Mantseva_Lab1\\visit.txt", std::ios_base::out | std::ios_base::app);
//             visitor <<this->id("visit.txt")+1<<"|"<<_reg_login<<"|"<<_reg_password<<"|"<<_reg_fio<<"|"<<_reg_mail<<"|"<<_reg_telefon<<"||\n";
//             visitor.close();
//             std::ofstream user;
//             user.open("C:\\mantseva-181-331-2semestr\\181-331_Mantseva_Lab1\\users.txt",std::ios_base::out | std::ios_base::app);
//             user << "visitor|"<<_reg_login<<"|"<<_reg_password<<"||\n";
//             user.close();
            QFile user("C:\\mantseva-181-331-2semestr\\181-331_Mantseva_Lab1\\visit.txt");
            if(user.open(QIODevice::Append | QIODevice::Text)){
                QTextStream write(&user);
                write << this->id("visit.txt")+1<<"|"<<reg_login<<"|"<<reg_password<<"|"<<reg_fio<<"|"<<reg_mail<<"|"<<reg_telefon<<"||\n";
                user.close();
            }
            QFile all("C:\\mantseva-181-331-2semestr\\181-331_Mantseva_Lab1\\users.txt");
            if (all.open( QIODevice::Append | QIODevice::Text)){
                QTextStream rite(&all);
                rite << "visitor|"<<reg_login<<"|"<<reg_password<<"||\n";
                all.close();
            }

             QMessageBox::information(this,"Поздравляем!", "Вы успешно зарегистрировались на выставку. Теперь вы можете войти в свою учётную запись");
    }
        else {
            QMessageBox::warning(this, "Что-то пошло не так", "Возможно вы не выбрали метку стендист-посетитель. Пожалуйста проверьте.");
        }
        this -> hide();
    }

}







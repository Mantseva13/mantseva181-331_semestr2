#-------------------------------------------------
#
# Project created by QtCreator 2019-02-27T21:07:19
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = 181-331_Mantseva
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        main.cpp \
    registration.cpp \
    MainWindow.cpp \
    autorization.cpp \
    admin.cpp \
    stendi.cpp \
    list_visit.cpp \
    list_stend.cpp \
    new_admin.cpp \
    excurs.cpp \
    list_st.cpp \
    list_ex.cpp \
    redakt_st.cpp \
    private_kabinet.cpp

HEADERS += \
    registration.h \
    MainWindow.h \
    autorization.h \
    admin.h \
    stendi.h \
    list_visit.h \
    list_stend.h \
    new_admin.h \
    excurs.h \
    list_st.h \
    list_ex.h \
    redakt_st.h \
    private_kabinet.h

FORMS += \
    registration.ui \
    MainWindow.ui \
    autorization.ui \
    admin.ui \
    stendi.ui \
    list_visit.ui \
    list_stend.ui \
    new_admin.ui \
    excurs.ui \
    list_st.ui \
    list_ex.ui \
    redakt_st.ui \
    private_kabinet.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

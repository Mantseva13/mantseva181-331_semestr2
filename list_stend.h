#ifndef LIST_STEND_H
#define LIST_STEND_H

#include <QDialog>
#include <QStandardItemModel>
#include <QStandardItem>
#include "string.h"

namespace Ui {
class list_stend;
}

using namespace  std;
struct stendist_dannie{
     string FIO;
     string mail;
     string telefon;
};

class list_stend : public QDialog
{
    Q_OBJECT

public:
    explicit list_stend(QWidget *parent = nullptr);
    ~list_stend();

private:
    Ui::list_stend *ui;
    QStandardItemModel *tab;
};

#endif // LIST_STEND_H

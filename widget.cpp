#include "widget.h"
#include "ui_widget.h"

widget::widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::widget)
{
    ui->setupUi(this);
    setAttribute(Qt::WA_DeleteOnClose, true);
}

widget::~widget()
{
    delete ui;
}

#ifndef AUTORIZATION_H
#define AUTORIZATION_H

#include <QDialog>

namespace Ui {
class autorization;
}

class autorization : public QDialog
{
    Q_OBJECT

public:
    explicit autorization(QWidget *parent = nullptr);
    ~autorization();

private slots:


    void on_pushButton_4_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_clicked();

private:
    Ui::autorization *ui;
};

#endif // AUTORIZATION_H

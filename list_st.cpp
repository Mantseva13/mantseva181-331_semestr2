#include "list_st.h"
#include "ui_list_st.h"
#include "string.h"
#include <fstream>
#include <iostream>
#include <QModelIndex>
#include <sstream>
#include <cstdlib>
#include "registration.h"

list_st::list_st(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::list_st)
{
    ui->setupUi(this);
    registration num;
    tab = new QStandardItemModel(num.id("red_st.txt"), 4, this);
    ui->tableView->setModel(tab);
    this->setWindowTitle("Список стэндов");
    tab->setHeaderData(0, Qt::Horizontal, "Название стэнда");
    tab->setHeaderData(1, Qt::Horizontal, "Владелец стэнда");
    tab->setHeaderData(2, Qt::Horizontal, "Дата");
    tab->setHeaderData(3, Qt::Horizontal, "Время");
    ui->tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    QModelIndex index;
    QString Qinfo;
    QString Qinfo2;
    QString Qinfo3;
    int num_info = 1;
    int num_strok = 0;
    string line, str;
    std::ifstream file;
    form_stendov info;

   file.open("C:\\mantseva-181-331-2semestr\\181-331_Mantseva_Lab1\\red_st.txt", std::ios::in);
    while (!file.eof()){
        getline(file,str);
        istringstream strok(str);
        while(getline(strok, line, '|')){
                if (num_info == 1){
                   info.person = line;
                  Qinfo = QString::fromLocal8Bit(info.person.c_str());
                index = tab->index(num_strok, 1);
                tab->setData(index, Qinfo);
                }
                if (num_info == 2) {
                    info.name = line;
                    Qinfo = QString::fromLocal8Bit(info.name.c_str());
                   index = tab->index(num_strok, 0);
                    tab->setData(index, Qinfo);
                }
                if (num_info == 3) {
                    info.day = line;
                    Qinfo = QString::fromLocal8Bit(info.day.c_str());
                }
                if (num_info == 4) {
                    info.month = line;
                    Qinfo2 = QString::fromLocal8Bit(info.month.c_str());
                }
                if (num_info == 5) {
                    info.year = line;
                    Qinfo3 = QString::fromLocal8Bit(info.year.c_str());
                    Qinfo = Qinfo+" "+Qinfo2+" "+Qinfo3;
                    index = tab->index(num_strok, 2);
                     tab->setData(index, Qinfo);
                }
                if (num_info == 6) {
                    info.time = line;
                    Qinfo = QString::fromLocal8Bit(info.time.c_str());
                   index = tab->index(num_strok, 3);
                    tab->setData(index, Qinfo);
                }
              num_info++;
            }

            num_info = 1;
           num_strok++;
          }

}

list_st::~list_st()
{
    delete ui;
}

#include "excurs.h"
#include "ui_excurs.h"
#include "registration.h"
#include "string.h"
#include <string>
#include <QMessageBox>
#include <fstream>
#include <iostream>
#include <QFile>
#include <QTextStream>

excurs::excurs(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::excurs)
{
    ui->setupUi(this);
}

excurs::~excurs()
{
    delete ui;
}

void excurs::on_lineEdit_3_textChanged(const QString &arg1) // название
{
    _name = arg1;
}

void excurs::on_lineEdit_2_textChanged(const QString &arg1) // продолжительность
{
    _time = arg1;
}

void excurs::on_lineEdit_textChanged(const QString &arg1) // цена
{
    _money = arg1;

}

void excurs::on_pushButton_clicked() // кнопка добавления
{
    registration for_num;
//    std::ofstream exc;
//    exc.open("C:\\mantseva-181-331-2semestr\\181-331_Mantseva_Lab1\\excurs.txt",std::ios_base::out | std::ios_base::app);
//    exc << for_num.id("excurs.txt")+1 <<"|"<<_name<<"|"<<_time<<"|"<<_money<<"||\n";
    QFile exc("C:\\mantseva-181-331-2semestr\\181-331_Mantseva_Lab1\\excurs.txt");
    if (exc.open(QIODevice::Append | QIODevice::Text)){
        QTextStream write(&exc);
        write << for_num.id("excurs.txt")+1 <<"|"<<_name<<"|"<<_time<<"|"<<_money<<"||\n";
        exc.close();
    }

    QMessageBox::information(this,"Отлично!", "Новая экскурсия успешно добавлена.");
}

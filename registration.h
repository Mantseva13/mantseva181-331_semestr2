#ifndef REGISTRATION_H
#define REGISTRATION_H

#include <QDialog>
#include "MainWindow.h"

using namespace std;
namespace Ui {
class registration;
}

class registration : public QDialog
{
    Q_OBJECT

public:
      QString reg_fio;
      QString reg_login;
      QString reg_password;
      QString dabl_password;
      QString reg_mail;
      QString reg_telefon;

      static int id(string name_file);

      MainWindow *host;
      Stendist *shost;
      int vis = 0;

    explicit registration(QWidget *parent = nullptr);
    ~registration();

private slots:
    void on_lineEdit_textChanged(const QString &arg1);

    void on_lineEdit_2_textChanged(const QString &arg1);

    void on_lineEdit_4_textChanged(const QString &arg1);

    void on_lineEdit_3_textChanged(const QString &arg1);

    void on_lineEdit_5_textChanged(const QString &arg1);



    void on_pushButton_clicked();

    void on_lineEdit_6_textChanged(const QString &arg1);

    void on_radioButton_2_clicked(bool checked);

    void on_radioButton_clicked(bool checked);


private:
    Ui::registration *ui;

};

#endif // REGISTRATION_H

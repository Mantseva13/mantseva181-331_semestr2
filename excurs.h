#ifndef EXCURS_H
#define EXCURS_H

#include <QDialog>
#include "string.h"

namespace Ui {
class excurs;
}
using namespace std;
class excurs : public QDialog
{
    Q_OBJECT

public:
    explicit excurs(QWidget *parent = nullptr);
    ~excurs();

    QString _name;
    QString _time;
    QString _money;



private slots:
    void on_pushButton_clicked();

    void on_lineEdit_3_textChanged(const QString &arg1);

    void on_lineEdit_2_textChanged(const QString &arg1);

    void on_lineEdit_textChanged(const QString &arg1);

private:
    Ui::excurs *ui;
};

#endif // EXCURS_H
